package pl.mm.bucketest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BucketestApplication {

    public static void main(String[] args) {
        SpringApplication.run(BucketestApplication.class, args);
    }

}
